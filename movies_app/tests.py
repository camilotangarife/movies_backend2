import json
from datetime import datetime

import mock
from django.contrib.auth.models import User
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse

from movies_app.models import AppUser, Movie


class SessionTest(TestCase):
    """Tests for a user session, from login to logout"""

    def setUp(self):
        self.user = User(
            username='test@omnimovies.com.co',
            email='test@omnimovies.com.co',
            first_name='Test',
            last_name='OmniMovies'
        )
        self.user.set_password('tests')
        self.user.save()

        self.app_user = AppUser(
            fk_user=self.user,
            birthdate=datetime(1993, 11, 11),
            address='Test neighborhood'
        )
        self.app_user.save()

    def test_login_ok(self):
        response = self.client.post(
            path=reverse('movies_backend:login'),
            data=json.dumps({
                'username': self.user.username,
                'password': 'tests'
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 200)

    def test_login_fail(self):
        response = self.client.post(
            path=reverse('movies_backend:login'),
            data=json.dumps({
                'username': self.user.username,
                'password': 'testss'
            }),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)

    def test_token_authorized(self):
        login = self.client.post(
            path=reverse('movies_backend:login'),
            data=json.dumps({
                'username': self.user.username,
                'password': 'tests'
            }),
            content_type='application/json'
        )
        token_dict = json.loads(login.content.decode('utf-8'))

        headers = {'HTTP_AUTHORIZATION': f'Bearer {token_dict["token"]}'}

        response = self.client.get(reverse('movies_backend:my-movies'), **headers)

        self.assertEqual(response.status_code, 200)

    def test_token_unauthorized(self):
        login = self.client.post(
            path=reverse('movies_backend:login'),
            data=json.dumps({
                'username': self.user.username,
                'password': 'tests'
            }),
            content_type='application/json'
        )
        token_dict = json.loads(login.content.decode('utf-8'))

        headers = {'HTTP_AUTHORIZATION': f'Bearer {token_dict["token"]}s'}

        response = self.client.get(reverse('movies_backend:my-movies'), **headers)

        self.assertEqual(response.status_code, 401)


class SignUpTest(TestCase):
    """This class tests de signup method"""
    def setUp(self):
        self.signup_data = {
            'email': 'tests@omnimovies.com',
            'password': 'test',
            'password2': 'test',
            'name': 'Tests',
            'lastname': 'OmniMovies',
            'birthdate': '1993-11-11',
            'address': 'Test address'
        }

    def test_signup_ok(self):
        response = self.client.put(
            path=reverse('movies_backend:signup'),
            data=json.dumps(self.signup_data)
        )
        self.assertEqual(response.status_code, 200)

        login = self.client.post(
            path=reverse('movies_backend:login'),
            data=json.dumps({
                'username': self.signup_data['email'],
                'password': self.signup_data['password']
            }),
            content_type='application/json'
        )
        self.assertEqual(login.status_code, 200)

    def test_signup_existing_user(self):
        self.client.put(
            path=reverse('movies_backend:signup'),
            data=json.dumps(self.signup_data)
        )
        response = self.client.put(
            path=reverse('movies_backend:signup'),
            data=json.dumps(self.signup_data)
        )
        self.assertEqual(response.status_code, 400)

    def test_signup_bad_request(self):
        keys_bad_request = ['email', 'password', 'name', 'lastname', 'birthdate', 'address']
        for key in keys_bad_request:
            bad_signup_data1 = self.signup_data.copy()
            del bad_signup_data1[key]
            test1 = self.client.put(
                path=reverse('movies_backend:signup'),
                data=json.dumps(bad_signup_data1)
            )
            self.assertEqual(test1.status_code, 400)

            bad_signup_data2 = self.signup_data.copy()
            bad_signup_data2[key] = ''
            test2 = self.client.put(
                path=reverse('movies_backend:signup'),
                data=json.dumps(bad_signup_data2)
            )
            self.assertEqual(test2.status_code, 400)

    def test_mismatch_passwords(self):
        mismatch_passwords_data = self.signup_data.copy()
        mismatch_passwords_data['password2'] = '123456'
        response = self.client.put(
            path=reverse('movies_backend:signup'),
            data=json.dumps(mismatch_passwords_data)
        )
        self.assertEqual(response.status_code, 400)


class MyMoviesTest(TestCase):
    """This class test the creation and retrieving of my movies"""
    def setUp(self):
        self.user = User(
            username='test@omnimovies.com.co',
            email='test@omnimovies.com.co',
            first_name='Test',
            last_name='OmniMovies'
        )
        self.user.set_password('tests')
        self.user.save()

        self.app_user = AppUser(
            fk_user=self.user,
            birthdate=datetime(1993, 11, 11),
            address='Test neighborhood'
        )
        self.app_user.save()

        login = self.client.post(
            path=reverse('movies_backend:login'),
            data=json.dumps({
                'username': self.user.username,
                'password': 'tests'
            }),
            content_type='application/json'
        )
        token_dict = json.loads(login.content.decode('utf-8'))

        self.headers = {'HTTP_AUTHORIZATION': f'Bearer {token_dict["token"]}'}

        file_mock = mock.MagicMock(spec=File, name='FileMock')
        file_mock.name = 'test1.jpg'

        self.movie = Movie(
            title='Test Movie',
            slug='test-movie',
            year=2015,
            summary='Test summary',
            director='Director',
            writers='Writer 1',
            actors_actresses='Actor1, Actress 1',
            stars=5,
            preview_image=file_mock,
            image=file_mock,
            created_by=self.app_user
        )
        self.movie.save()

        self.movie2 = Movie(
            title='Test Movie2',
            slug='test-movie2',
            year=2015,
            summary='Test summary',
            director='Director',
            writers='Writer 1',
            actors_actresses='Actor1, Actress 1',
            stars=5,
            preview_image=file_mock,
            image=file_mock
        )
        self.movie2.save()

    def test_create_movie(self):
        image = SimpleUploadedFile('image.jpg', content=None, content_type='image/jpeg')

        new_title = 'Movie 1'
        response = self.client.post(
            path=reverse('movies_backend:my-movies'),
            data={
                'title': new_title,
                'year': 2010,
                'summary': 'Summary 1',
                'director': 'Director 1',
                'writers': 'Writer 1',
                'actors_actresses': 'Actors',
                'stars': 2,
                'preview_image': image,
                'image': image,
            },
            **self.headers
        )

        self.assertEqual(response.status_code, 200)

        movies_response = self.client.get(reverse('movies_backend:my-movies'), **self.headers)
        movies = json.loads(movies_response.content.decode('utf-8'))['movies']

        result = False
        for m in movies:
            if m['title'] == new_title:
                result = True

        self.assertEqual(result, True)

    def test_create_existing_movie(self):
        image = SimpleUploadedFile('image.jpg', content=None, content_type='image/jpeg')

        new_title = self.movie.title
        response = self.client.post(
            path=reverse('movies_backend:my-movies'),
            data={
                'title': new_title,
                'year': 2010,
                'summary': 'Summary 1',
                'director': 'Director 1',
                'writers': 'Writer 1',
                'actors_actresses': 'Actors',
                'stars': 2,
                'preview_image': image,
                'image': image,
            },
            **self.headers
        )

        self.assertEqual(response.status_code, 400)

        movies_response = self.client.get(reverse('movies_backend:my-movies'), **self.headers)
        movies = json.loads(movies_response.content.decode('utf-8'))['movies']

        num_movies = 0
        for m in movies:
            if m['title'] == new_title:
                num_movies += 1

        self.assertEqual(num_movies, 1)

    def delete_existing_movie(self):
        slug = self.movie.slug
        response = self.client.delete(
            path=reverse('movies_backend:delete-movie', kwargs={'slug': slug}),
            **self.headers
        )

        self.assertEqual(response.status_code, 200)

        movies_response = self.client.get(reverse('movies_backend:my-movies'), **self.headers)
        movies = json.loads(movies_response.content.decode('utf-8'))['movies']

        num_movies = 0
        for m in movies:
            if m['slug'] == slug:
                num_movies += 0

        self.assertEqual(num_movies, 0)

    def delete_inexisting_movie(self):
        slug = self.movie.slug + '1'
        response = self.client.delete(
            path=reverse('movies_backend:delete-movie', kwargs={'slug': slug}),
            **self.headers
        )

        self.assertEqual(response.status_code, 400)

    def get_my_movies(self):
        my_movies_count = 0
        others_movies_count = 0

        movies_response = self.client.get(reverse('movies_backend:my-movies'), **self.headers)
        movies = json.loads(movies_response.content.decode('utf-8'))['movies']

        for m in movies:
            if m['slug'] == self.movie.slug:
                my_movies_count += 1
            elif m['slug'] == self.movie2.slug:
                others_movies_count += 1

        self.assertEqual(my_movies_count, 1)
        self.assertEqual(others_movies_count, 0)


class MovieDetailsTest(TestCase):
    """This class test the retrieve of the details of a movie"""
    def setUp(self):
        file_mock = mock.MagicMock(spec=File, name='FileMock')
        file_mock.name = 'test1.jpg'
        self.movie = Movie(
            title='Test Movie2',
            slug='test-movie2',
            year=2015,
            summary='Test summary',
            director='Director',
            writers='Writer 1',
            actors_actresses='Actor1, Actress 1',
            stars=5,
            preview_image=file_mock,
            image=file_mock
        )
        self.movie.save()

    def test_existing_movie(self):
        response = self.client.get(
            path=reverse('movies_backend:movie-details', kwargs={'slug': self.movie.slug})
        )
        self.assertEqual(response.status_code, 200)

    def test_unexisting_movie(self):
        response = self.client.get(
            path=reverse('movies_backend:movie-details', kwargs={'slug': 'random-movie'})
        )
        self.assertEqual(response.status_code, 404)
